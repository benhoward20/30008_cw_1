from numerical_shooting import *
import numpy as np
import matplotlib.pyplot as plt
import sys

# Define ODEs
def hopf(X, t, beta, sigma):
	params = [beta, sigma]
	u1_dot = params[0] * X[0] - X[1] + params[1] * X[0] * (X[0]**2 + X[1]**2)
	u2_dot = X[0] + params[0] * X[1] + params[1] * X[1] * (X[0]**2 + X[1]**2)
	
	return [u1_dot, u2_dot]

def hopf_mod(X, t, beta, sigma):
	params = [beta, sigma]
	u1_dot = params[0] * X[0] - X[1] + X[0] * (X[0]**2 + X[1]**2) - X[0] * (X[0]**2 + X[1]**2)**2
	u2_dot = X[0] + params[0] * X[1] + X[1] * (X[0]**2 + X[1]**2) - X[1] * (X[0]**2 + X[1]**2)**2
	
	return [u1_dot, u2_dot]


# Define phase conditions	
def phase_hopf(X, params):
	phi = params[0] * X[0] - X[1] + params[1] * X[0] * (X[0]**2 + X[1]**2)
	return phi

def phase_hopf_mod(X, params):
	phi = params[0] * X[0] - X[1] + X[0] * (X[0]**2 + X[1]**2) - X[0] * (X[0]**2 + X[1]**2)**2
	return phi
	

if __name__ == '__main__':
	'''
	Demo File
	
		- Call from the command line with either "--natural_param" or "--pseudo_arc" to demonstrate the functionality of the code.
	'''

	u = np.array([0.3, 0, 6.3])
	
	if sys.argv[1] == "--natural_param":
		params = np.array([[2, 0], -1], dtype=object)
		sol = natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True)
		print(sol)
	elif sys.argv[1] == "--pseudo_arc":
		params = np.array([[2, -1], -1], dtype=object)
		sol = pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True)
		print(sol)
	