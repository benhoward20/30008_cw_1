from numerical_shooting import *
import numpy as np
import sys
from scipy.integrate import odeint

def hopf(X, t, beta, sigma):
	params = [beta, sigma]
	u1_dot = params[0] * X[0] - X[1] + params[1] * X[0] * (X[0]**2 + X[1]**2)
	u2_dot = X[0] + params[0] * X[1] + params[1] * X[1] * (X[0]**2 + X[1]**2)
	
	return [u1_dot, u2_dot]
	
def hopf_mod(X, t, beta, sigma):
	params = [beta, sigma]
	u1_dot = params[0] * X[0] - X[1] + X[0] * (X[0]**2 + X[1]**2) - X[0] * (X[0]**2 + X[1]**2)**2
	u2_dot = X[0] + params[0] * X[1] + X[1] * (X[0]**2 + X[1]**2) - X[1] * (X[0]**2 + X[1]**2)**2
	
	return [u1_dot, u2_dot]
	
def phase_hopf(X, params):
	phi = params[0] * X[0] - X[1] + params[1] * X[0] * (X[0]**2 + X[1]**2)
	return phi

def phase_hopf_mod(X, params):
	phi = params[0] * X[0] - X[1] + X[0] * (X[0]**2 + X[1]**2) - X[0] * (X[0]**2 + X[1]**2)**2
	return phi
	
def hopf_an(params, t):
	u_1 = np.sqrt(params[0]) * np.cos(t)
	u_2 = np.sqrt(params[0]) * np.sin(t)
	
	return [u_1, u_2]

	
def test_shooting_output(u):
	params = np.array([2, -1], dtype=object)
	numeric_sol = shooting(u, phase_hopf, hopf, params)
	analytics = hopf_an(params, numeric_sol[-1])
	
	output = np.isclose(numeric_sol[:-1], analytics, atol=1e-5)
	if output.all() == True:
		print("Output of shooting algorithm passed test.")
	else:
		print("Output of shooting algorithm failed test.")
	return None
	
def test_natural_output(u):
	params = np.array([[2, 0], -1], dtype=object)
	vary_p = 0
	sol = natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=False)
	
	p_span = np.linspace(params[vary_p][0], params[vary_p][1], 101)
	root_p = []
	for i in p_span:
		root_p.append(np.sqrt(i))
	
	max_mag_list = []
	p_list = []
	for line in sol:
		params[vary_p] = line[-1]
		solutions = odeint(hopf, line[:-2], np.linspace(0, line[-2]), args=tuple(params))
		mag_list = []
		for row in solutions:
			mag_list.append(np.linalg.norm(row))
		max_mag_list.append(max(mag_list))
		p_list.append(params[vary_p])
	
	test_list = []
	for i, j in zip(root_p, max_mag_list):
		output = np.isclose(i, j, atol=1e-3)
		test_list.append(output)
	test = np.array(test_list)
	if test.all() == True:
		print("Output of natural parameter algorithm passed test.")
	else:
		print("Output of natural parameter algorithm failed test.")
	return None
	
def long_u():
	u = np.array([0.3, 0, 6.3, 1])
	params = np.array([[2, 0], -1], dtype=object)
	if natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too many state variables input to natural parameter passed.")
	else:
		print("Test for too many state variables input to natural parameter failed.")
	
	params = np.array([[2, -1], -1], dtype=object)
	if pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too many state variables input to pseudo-arclength passed.")
	else:
		print("Test for too many state variables input to pseudo-arclength failed.")

def short_u():
	u = np.array([0.3, 0])
	params = np.array([[2, 0], -1], dtype=object)
	if natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too few state variables input to natural parameter passed.")
	else:
		print("Test for too few state variables input to natural parameter failed.")
	
	params = np.array([[2, -1], -1], dtype=object)
	if pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too few state variables input to pseudo-arclength passed.")
	else:
		print("Test for too few state variables input to pseudo-arclength failed.")

def long_p():
	u = np.array([0.3, 0, 6.3])
	params = np.array([[2, 0], -1, 1], dtype=object)
	if natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too many parameters input to natural parameter passed.")
	else:
		print("Test for too many parameters input to natural parameter failed.")
	
	params = np.array([[2, -1], -1, 1], dtype=object)
	if pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too many parameters input to pseudo-arclength passed.")
	else:
		print("Test for too many parameters input to pseudo-arclength failed.")

def short_p():
	u = np.array([0.3, 0, 6.3])
	params = np.array([[2, 0]], dtype=object)
	if natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too few parameters input to natural parameter passed.")
	else:
		print("Test for too few parameters input to natural parameter failed.")
	
	params = np.array([[2, -1]], dtype=object)
	if pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for too few parameters input to pseudo-arclength passed.")
	else:
		print("Test for too few parameters input to pseudo-arclength failed.")

def ind_p():
	u = np.array([0.3, 0, 6.3])
	params = np.array([-1, [2, 0]], dtype=object)
	if natural_param(u, params, hopf, phase_cond=phase_hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for incorrect parameter index input to natural parameter passed.")
	else:
		print("Test for incorrect parameter index input to natural parameter failed.")
	
	params = np.array([-1, [2, -1]], dtype=object)
	if pseudo_arc(u, params, hopf_mod, phase_cond=phase_hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for incorrect parameter index input to pseudo-arclength passed.")
	else:
		print("Test for incorrect parameter index input to pseudo-arclength failed.")
		
def no_phase():
	u = np.array([0.3, 0, 6.3])
	params = np.array([[2, 0], -1], dtype=object)
	if natural_param(u, params, hopf, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for no phase condition input to natural parameter passed.")
	else:
		print("Test for no phase condition input to natural parameter failed.")
	
	params = np.array([[2, -1], -1], dtype=object)
	if pseudo_arc(u, params, hopf_mod, discretisation=shooting, vary_p=0, plot=True) == 1:
		sys.stderr.flush()
		print("Test for no phase condition input to pseudo-arclength passed.")
	else:
		print("Test for no phase condition input to pseudo-arclength failed.")

if __name__ == '__main__':
	u = np.array([0.3, 0, 6.3])
	test_shooting_output(u)
	test_natural_output(u)
	
	long_u()
	short_u()
	long_p()
	short_p()
	ind_p()
	no_phase()