import numpy as np
import scipy.optimize as op
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import sys
import warnings
warnings.filterwarnings("ignore")

def solve(u, T, ode_func, params):
	'''
	solve:
		- Function to return the value of each input variable at time t = T.
	
	Inputs:
			u - ndarray - Vector of guesses of state variables [x0, y0]
			T - float - Initial guess of time period
			ode_func - function - Function handle specifying ODEs
			params - tuple - Vector of parameters
	Outputs:
			u_T - ndarray - Updated approximation of state variables [x, y]
	'''
	t = np.linspace(0, T)
	solutions = odeint(ode_func, u, t, args=params)
	
	u_T = solutions[-1,:]
	return u_T

def constraints(v, phase_cond, ode_func, params, dv=None, v_tilde=None, vary_p=None, pseudo=False):
	'''
	constraints:
		- Function defining constraint equations to govern the various numerical methods.
	
	Inputs:
			v - ndarray -Vector of guesses of state variables, time period, and parameter (where pseudo = True) [x0, y0, T, p]
			phase_cond - function - Function handle specifying a phase condition
			ode_func - function - Function handle specifying ODEs
			params - ndarray - Vector of parameters
			dv - ndarray - Difference between v_t-1 and v_t-2
			v_tilde - ndarray - Approximation to v_t
			vary_p - int - Index of parameter to vary_p
			pseudo - boolean - Boolean specifying whether pseudo arclength continuation is being performed
	Outputs:
			phi - ndarray - Residual from each constraint
	'''
	if pseudo == False:
		u = v[:-1]
		T = v[-1]
	elif pseudo == True:
		u = v[:-2]
		T = v[-2]
		p = v[-1]
		params = np.insert(params, vary_p, v[-1])
	
	params = tuple(params)
	phi = u - solve(u, T, ode_func, params) # Difference in variable values
	
	phase = phase_cond(np.hstack((u, T)), params)
	phi = np.hstack((phi, phase))
	
	if pseudo == True:
		secant = np.dot(dv, v - v_tilde)
		phi = np.hstack((phi, secant))
	
	return phi

def shooting(v, phase_cond, ode_func, params, dv=None, v_tilde=None, vary_p=None, pseudo=False):
	'''
	Function that performs numerical shooting on a system using a given initial guess and phase condition.
	
	Inputs:
			v - ndarray - Vector of guesses of state variables, time period, and parameter (where pseudo = True) [x0, y0, T, p]
			phase_cond - function - Function handle specifying a phase condition
			ode_func - function - Function handle specifying ODEs
			params - ndarray - Vector of parameters
			dv - ndarray - Difference between v_t-1 and v_t-2
			v_tilde - ndarray - Approximation to v_t
			vary_p - int - Index of parameter to vary_p
			pseudo - boolean - Boolean specifying whether pseudo arclength continuation is being performed
	Outputs:
			updated_u - ndarray - Vector of updated state variables, time period, and parameter (where pseudo = True) [x, y, T, p]
	'''
	updated_u = op.fsolve(constraints, v, args=(phase_cond, ode_func, params, dv, v_tilde, vary_p, pseudo))
	
	return updated_u
	
def plot_max(ode_func, u, T, params):
	'''
	Function to find the maximum value of ||u|| to plot.
	
	Inputs:
			ode_func - function - Function handle specifying ODEs
			u - ndarray - Vector of corrected guesses of starting variables
			T - float - Corrected guess of time period
			params - ndarray - Vector of parameters
	Outputs:
			max_u - float - The maximum value of ||u|| to plot
	'''
	solutions = odeint(ode_func, u, np.linspace(0, T), args=tuple(params))
	mag_list = []
	for row in solutions:
		mag_list.append(np.linalg.norm(row))
		
	max_u = max(mag_list)
	return max_u
	
def root_find(u_tilde, phase_cond, ode_func, params, dv=None, v_tilde=None, vary_p=None, pseudo=True):
	'''
	Function that performs root finding on a system using a given initial guess.
	
	Inputs:
			u_tilde - ndarray - Vector of guesses of state variables, time period, and parameter (where pseudo = True) [x0, y0, T, p]
			phase_cond - function - Function handle specifying a phase condition
			ode_func - function - Function handle specifying ODEs
			params - ndarray - Vector of parameters
			dv - ndarray - Difference between v_t-1 and v_t-2
			v_tilde - ndarray - Approximation to v_t
			vary_p - int - Index of parameter to vary_p
			pseudo - boolean - Boolean specifying whether pseudo arclength continuation is being performed
	Outputs:
			updated_u - ndarray - Vector of updated state variables, time period, and parameter (where pseudo = True) [x, y, T, p]
	'''
	roots = op.fsolve(ode_func, u_tilde, args=(tuple(params)))
	
	return roots

def natural_param(u0_tilde, params, ode_func, phase_cond=0, vary_p=0, discretisation=root_find, plot=False, steps=100):
	'''
	Function that performs natural parameter continuation on a system using a given initial guess and phase condition.
	
	Inputs:
			u0_tilde - ndarray - Vector of guesses of state variables and time period [x0, y0, T]
			params - ndarray - Vector of parameters
			ode_func - function - Function handle specifying ODEs
			phase_cond - function - Function handle specifying a phase condition
			vary_p - int - Index of parameter to vary
			discretisation - function - Method of discretisation to implement, defaults to None
			plot - boolean - Boolean specifying whether solution is to be plotted
			steps - int - Number of parameter increments to make
	Outputs:
			solutions - ndarray - Numpy array of state variables, time period, and parameter at each parameter-step [u, T, p] 
	'''
	max_mag_list = []
	p_list = []
	solutions = []
	roots = []
	
	# Initially defines p0 as a list, where the parameter to be varied is set to its initial value.
	p_span = params[vary_p]
	# Test for parameter range.
	try:
		Delta = (p_span[1] - p_span[0]) / steps
	except TypeError:
		sys.stderr.write("The index specified by vary_p does not match the position of the parameter range.\n")
		return 1
	p0 = params
	p0 = np.delete(p0, vary_p, axis=0)
	p0 = np.insert(p0, vary_p, p_span[0])
	
	# Test for dimensionality.
	try:
		ode_output = ode_func(u0_tilde[:-1], u0_tilde[-1], *p0)
	except TypeError:
		sys.stderr.write("The number of parameters required does not match the number given.\n")
		return 1
	except IndexError:
		sys.stderr.write("The dimension of the initial guess array, u, is less than the dimension of the ODE function.\n")
		return 1
	if len(u0_tilde) - 1 != len(ode_output):
		sys.stderr.write("The dimension of the initial guess array, u, is greater than the dimension of the ODE function.\n")
		return 1
	for i in range(steps + 1):
		# Uses natural parameter continuation to correct the value of u0_tilde.
		try:
			u0 = discretisation(u0_tilde, phase_cond, ode_func, p0)
		except TypeError:
			sys.stderr.write("Missing phase condition.\n")
			return 1
		solutions.append(np.hstack((u0, p0[vary_p])))
		
		if discretisation.__name__ == 'shooting':
		# Finds the maximum value of ||u|| to plot.
			max_mag_list.append(plot_max(ode_func, u0[:-1], u0[-1], p0))
		elif discretisation.__name__ == 'root_find':
			roots.append(u0)
		p_list.append(p0[vary_p])
		
		# Defines the starting variables of the next guess.
		u0_tilde = np.copy(u0)
		p0[vary_p] = np.round(p0[vary_p] + Delta, 4)
	
	if plot == True:
		ax = plt.gca()
		if discretisation.__name__ == 'shooting':
			plt.plot(p_list, max_mag_list)
			ax.set_ylabel(r"||u||", fontsize=14)
		elif discretisation.__name__ == 'root_find':
			plt.plot(p_list, roots)
			ax.set_ylabel(r"Root", fontsize=14)
		ax.set_xlabel(r"Bifurcation Parameter", fontsize=14)
		ax.set_title(r"Bifurcation Diagram", fontsize=14)
		plt.show()
	
	return np.array(solutions)

def pseudo_arc(u0_tilde, params, ode_func, phase_cond=0, vary_p=0, discretisation=lambda a, u, b, c: u, plot=False, steps=100):
	'''
	Function that performs pseudo arclength continuation on a system using a given initial guess and phase condition.
	
	Inputs:
			u0_tilde - ndarray - Vector of guesses of state variables and time period [x0, y0, T]
			params - ndarray - Vector of parameters
			ode_func - function - Function handle specifying ODEs
			phase_cond - function - Function handle specifying a phase condition
			vary_p - int - Index of parameter to vary
			discretisation - function - Method of discretisation to implement, defaults to None
			plot - boolean - Boolean specifying whether solution is to be plotted
			steps - int - Number of parameter increments to make
	Outputs:
			solutions - ndarray - Numpy array of state variables, time period, and parameter at each parameter-step [u, T, p] 
	'''
	max_mag_list = []
	p_list = []
	solutions = []
	
	# Parameter to be varied is set to its initial value.
	# Step size is calculated.
	p_span = params[vary_p]
	
	# Test for parameter range.
	try:
		Delta = (p_span[1] - p_span[0]) / steps
	except TypeError:
		sys.stderr.write("The index specified by vary_p does not match the position of the parameter range.\n")
		return 1
		
	p0 = params
	
	# Test params type.
	try:
		p0[vary_p] = p_span[0]
	except TypeError:
		sys.stderr.write("Params must be defined as a NumPy array.\n")
		return 1
	
	# Test for dimensionality.
	try:
		ode_output = ode_func(u0_tilde[:-1], u0_tilde[-1], *params)
	except TypeError:
		sys.stderr.write("The number of parameters required does not match the number given.\n")
		return 1
	except IndexError:
		sys.stderr.write("The dimension of the initial guess array, u, is less than the dimension of the ODE function.\n")
		return 1
	if len(u0_tilde) - 1 != len(ode_output):
		sys.stderr.write("The dimension of the initial guess array, u, is greater than the dimension of the ODE function.\n")
		return 1
	
	# Initial guess is corrected using specified method of discretisation.
	try:
		u0 = discretisation(u0_tilde, phase_cond, ode_func, p0)
	except TypeError:
		sys.stderr.write("Missing phase condition.\n")
		return 1
	solutions.append(np.hstack((u0, p0[vary_p])))
	
	# Finds the maximum value of ||u|| to plot.
	max_mag_list.append(plot_max(ode_func, u0[:-1], u0[-1], p0))
	p_list.append(p0[vary_p])
	
	# Uses natural parameter continuation and specified method of discretisation to find and correct u1.
	p1 = np.copy(p0)
	p1[vary_p] += Delta
	u1 = discretisation(u0, phase_cond, ode_func, p1)
	solutions.append(np.hstack((u1, p1[vary_p])))
	
	# Finds the maximum value of ||u|| to plot
	max_mag_list.append(plot_max(ode_func, u1[:-1], u1[-1], p1))
	p_list.append(p1[vary_p])
	
	# Initialises v.
	# Removes the variable parameter from the params array.
	p0 = p0[vary_p]
	p1 = p1[vary_p]
	v0 = np.hstack((u0, p0))
	v1 = np.hstack((u1, p1))
	params = np.delete(params, vary_p)

	for i in range(steps - 1):
		# Uses pseudo arclength continuation and specified method of discretisation to find and correct v2.
		dv = v1 - v0
		v2_tilde = v1 + dv
		v2 = discretisation(v2_tilde, phase_cond, ode_func, params, dv, v2_tilde, vary_p, pseudo=True)
		
		# Finds the maximum value of ||u|| to plot.
		max_mag_list.append(plot_max(ode_func, v2[:-2], v2[-2], np.insert(params, vary_p, v2[-1])))
		p_list.append(v2[-1])
		solutions.append(v2)
		
		# Resets variables
		v0 = np.copy(v1)
		v1 = np.copy(v2)
	
	if plot == True:
		ax = plt.gca()
		plt.plot(p_list, max_mag_list)
		ax.set_xlabel(r"Bifurcation Parameter", fontsize=14)
		ax.set_ylabel(r"||u||", fontsize=14)
		ax.set_title(r"Bifurcation Diagram", fontsize=14)
		plt.show()
		
	return np.array(solutions)